"use strict";

global.$ = {
    path: {
        task: require('./gulp/path/tasks.js'),
        build: './build'
    },
    gulp: require('gulp'),
    del: require('del')
};

$.path.task.forEach(function (taskPath) {
    require(taskPath)();
});

$.gulp.task('build', $.gulp.series(
    'clean',
    $.gulp.parallel(
        'styles:build-min',
    )
));
$.gulp.task('default', $.gulp.series(
    'build',
    $.gulp.parallel(
        'watch'
    )
));