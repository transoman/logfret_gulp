let gp = require('gulp-load-plugins')(),
    autoprefixer = require('autoprefixer'),
    stylesPATH = {
        "input": "./app/sass/",
        "ouput": $.path.build + "/css/"
    };

module.exports = function () {
    $.gulp.task('styles:build-min', () => {
        return $.gulp.src(stylesPATH.input + 'style.sass')
            .pipe(gp.plumber())
            .pipe(gp.sass())
            .pipe(gp.postcss([
                autoprefixer({
                    browsers: ['last 5 versions'],
                    cascade: false
                })
            ]))
            .pipe(gp.csscomb())
            .pipe(gp.csso())
            .pipe(gp.rename('styles.min.css'))
            .pipe($.gulp.dest(stylesPATH.ouput))
    });
};